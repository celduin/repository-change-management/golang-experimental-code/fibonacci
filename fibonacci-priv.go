package fibonacci

import (
	"fmt"
)

/*Sequence - Function to print the desired number of fibonacci secquence items
Inputs:
 - count, int; The number of items in the sequence, from the start to be printed
*/
func sequence(count int) {
	var previous int
	var current int
	var tmp int

	for i := 0; i < count; i++ {
		switch i {
		case 0:
		case 2:
			//Do Nothing
			break
		case 1:
			current++
			break
		default:
			tmp = previous
			previous = current
			current = tmp + current
		}
		fmt.Println("Sequence Number", i+1, ": ", previous+current)
	}
}
