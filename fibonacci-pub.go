package fibonacci

import (
	"fmt"
)

/*Help is a function to print out help text when requested
 */
func Help() {
	fmt.Println("Fibonacci provides a means of printing the Fibonacci Sequence.")
	fmt.Println("You are prompted to input how far you want the sequence to run for.")
}

/*Start is a function to begin the process
It prompts for the length of the sequence to be run
then uses the sequence function to execute it
*/
func Start() {
	var count int
	fmt.Println("How long do you want the fibonacci series?: ")
	fmt.Scanln(&count)
	sequence(count)
}
